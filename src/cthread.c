#include <stdio.h>
#include <stdlib.h>
#include "../include/cthread.h"
#include "../include/cdata.h"
#include "../include/support.h"

#define MAX_RANGE 255


/*DECLARAÇÃO DAS FILAS E VARIAVEIS AUXILIARES*/
FILA2 filaApto;
FILA2 filaBloqueado;
int flagFilas = 0;
/*-------------------------------------------*/
/*VARIAVEL DO ESCALONADOR E THREAD MAIN*/
TCB_t threadMain;
TCB_t* threadAtual;
ucontext_t* contextoEscalonador = NULL;
int flagInit = 0;
int globalTid = 1;
int filaMutex = 0;
int semaforoInit = 0;
csem_t semaforo;
int naoDesbloqueia = 0;
/*-------------------------------------*/
//INIT DO ESCALONADOR
int inicializaEscalonador(){
	
	if (contextoEscalonador == NULL){
        contextoEscalonador = (ucontext_t*) malloc(sizeof(ucontext_t));
        if (contextoEscalonador== NULL) return -1; //erro no malloc
        
        contextoEscalonador->uc_link = NULL;
        contextoEscalonador->uc_stack.ss_sp = (char*) malloc(SIGSTKSZ);
        contextoEscalonador->uc_stack.ss_size = SIGSTKSZ;
        
        getcontext(contextoEscalonador);
        makecontext(contextoEscalonador, (void (*)(void)) escalonador, 0, NULL);
    }

	//CRIA CONTEXTO DA MAIN
	threadMain.context.uc_stack.ss_sp = (char *)malloc(sizeof(SIGSTKSZ));
	if(threadMain.context.uc_stack.ss_sp == NULL)
		return -1;
	threadMain.context.uc_stack.ss_size = SIGSTKSZ;
	threadMain.state = -1;
	threadMain.tid = 0;
	threadMain.ticket = rand() % MAX_RANGE;
	threadAtual = &threadMain; //MAIN É A THREAD ATUAL
	return 0;

}

/* INICIALIZA A BIBLIOTECA | PRECISA SER CHAMADA EM TODOS OS MÉTODOS, POIS CRIA AS FILAS E INICIALIZA O ESCALONADOR */
int initLib(){
	if(flagInit == 0){
		flagInit = 1; //FLAG PARA INICIAR APENAS UMA VEZ
		if(CreateFila2(&filaApto) != 0){
			return -1; //CRIA AS FILAS NECESSÁRIAS
		}
		printf("Fila de Apto criada! \n");
		if(CreateFila2(&filaBloqueado) != 0){
			return -1;
		}
		printf("Fila de Bloqueado criada! \n");
		if(inicializaEscalonador() == -1)//INICIALIZA O ESCALONADOR
			return -1;
	}
	return 0;
}
/* FUNÇÃO QUE ESCOLHE QUAL SERÁ A THREAD ESCOLHIDA*/
TCB_t* ticketPremiado(){
	TCB_t *nextThread = NULL;
	TCB_t *aux;
	int auxTicket = MAX_RANGE;
	int ticket = rand() % MAX_RANGE;
	//printf("Ticket escolhido: %d \n", ticket);
	/*PEGA A PROXIMA THREAD*/
	FirstFila2(&filaApto);
	aux = GetAtIteratorFila2(&filaApto);
	//printf("ticket: %d\n", ticket);
	while( aux != NULL ){
		//printf("ID da thread: %d Ticket: %d \n", Thread->tid, Thread->ticket);
		if(abs(aux->ticket-ticket) < auxTicket){
			auxTicket = abs(ticket-aux->ticket);
			nextThread = aux;
		}
		NextFila2(&filaApto);
		aux = GetAtIteratorFila2(&filaApto);
	}

	/*DELETA A THREAD DA FILA DE APTOS E JOGA NA DE EXECUÇÃO*/
	if(nextThread != NULL){
		FirstFila2(&filaApto);
		aux = GetAtIteratorFila2(&filaApto);
		while( aux != NULL ){
			//printf("tid Thread: %d\n", Thread->tid);
			//printf("tid nextThread: %d\n", nextThread->tid);
			if(nextThread->tid == aux->tid){
				DeleteAtIteratorFila2(&filaApto);
			}
			NextFila2(&filaApto);
			aux = GetAtIteratorFila2(&filaApto);
		}
	}
	/* QUALQUER SEGMENTATION FAULT, DELETAR */
	free(aux);
	/* QUALQUER SEGMENTATION FAULT, DELETAR */
	return nextThread;
}
/* FUNÇÃO QUE DESBLOQUEIA UMA THREAD BLOQUEADA QUANDO A MESMA JA PODE SER LIBERADA */
int desbloqueiaThread(int tid){
	TCB_t *aux;
	FirstFila2(&filaBloqueado);
	aux = GetAtIteratorFila2(&filaBloqueado);
	while( aux != NULL ){
		if(tid == aux->tid){
			//printf("ENTROU PRA DELETAR\n");
			if(AppendFila2(&filaApto, aux)){ //salva a thread criada na fila de aptos
				printf("Append deu merda\n");
				return -1;
			}
			DeleteAtIteratorFila2(&filaBloqueado);

			return 0;
		}
		NextFila2(&filaBloqueado);
		aux = GetAtIteratorFila2(&filaBloqueado);
	}
	return -1;
}


//FUNÇÃO QUE IRÁ ESCOLHER QUAL THREAD SERÁ EXECUTADA NO MOMENTO
void escalonador(){

	/* VERIFICA SE THREAD ATUAL ESTÁ BLOQUEANDO ALGUÉM */
	/* CASO VERDADE, DESBLOQUEIA A THREAD BLOQUEADA */
	if(threadAtual->state > -1 && naoDesbloqueia == 0){
		desbloqueiaThread(threadAtual->state);
	}
	//printf("Entrou escalonador \n");
	naoDesbloqueia = 0;

	threadAtual = NULL;
	/*GERA O TICKET PREMIADO E FAZ A ESCOLHA DA THREAD QUE SERÁ EXECUTADA*/
	threadAtual = ticketPremiado();
	/* caso nenhuma thread seja escolhida (todas terminadas), fecha o escalonador */
	if(threadAtual == NULL){
		exit(0);
	}//SETA THREAD ATUAL COMO EXECUTANDO
    //printf("Thread Escolhida->tid: %d Ticket: %d State: %d\n", threadAtual->tid, threadAtual->ticket, threadAtual->state);
	setcontext(&threadAtual->context);
}

/* FUNÇÃO QUE LIMPA UMA FILA */
int	ClearFila2(PFILA2 pFila) {
	FirstFila2(pFila);
	while (GetAtIteratorFila2(pFila)!=NULL) {
		if (DeleteAtIteratorFila2(pFila)) {
			return -1;
		}
	}
	return 0;
}

/* CRIA UMA THREAD E A JOGA NA FILA DE APTOS */
int ccreate (void* (*start)(void*), void *arg){
	if(initLib() == -1)
		return -1;
			
    TCB_t *newThread = malloc(sizeof(TCB_t));
    if (newThread == NULL)
        return -1;
        
    newThread->tid = globalTid++;
	newThread->state = -1;
	newThread->ticket = rand() % MAX_RANGE;  
	
	getcontext(&newThread->context);
	
    newThread->context.uc_link = contextoEscalonador;
    newThread->context.uc_stack.ss_sp = (char*) malloc(SIGSTKSZ);
    if(newThread->context.uc_stack.ss_sp == NULL){
        return -1; //erro no malloc
    }
    newThread->context.uc_stack.ss_size = SIGSTKSZ;
    
    makecontext(&newThread->context, (void(*)(void))start, 1, arg);
    if(AppendFila2(&filaApto, newThread)){ //salva a thread criada na fila de aptos
		printf("Append deu merda\n");
	}
	
	return newThread->tid;
}

/*uma thread pode liberar a CPU de forma voluntária com o auxílio da primitiva
cyield. Se isso acontecer, a thread que executou cyield retorna ao estado apto, sendo reinserida na fila de apto. Então, o
escalonador será chamado para selecionar a thread que receberá a CPU. */

/* LIBERA A CPU POR VONTADE PRÓPRIA */
int cyield(void){

	if(threadAtual == NULL){
		return -1;
	}

	TCB_t *threadAnterior;
	threadAnterior = threadAtual;

	//printf("Entrei4 aqui \n");
	if(AppendFila2(&filaApto, threadAnterior)){ //salva a thread criada na fila de aptos
		printf("Append deu merda\n");
	}

	naoDesbloqueia = 1;
	swapcontext(&threadAnterior->context, contextoEscalonador);

	return 0;
}


/*Sincronização de término: uma thread pode ser bloqueada até que outra termine sua execução usando a função cjoin. A
função cjoin recebe como parâmetro o identificador da thread cujo término está sendo aguardado. Quando essa thread
terminar, a função cjoin retorna com um valor inteiro indicando o sucesso ou não de sua execução. Uma determinada
thread só pode ser esperada por uma única outra thread. Se duas ou mais threads fizerem cjoin para uma mesma thread,
apenas a primeira que realizou a chamada será bloqueada. As outras chamadas retornarão imediatamente com um código
de erro. Se cjoin for feito para uma thread que não existe (não foi criada ou já terminou), a função retornará
imediatamente com o código de erro. Observe que não há necessidade de um estado zombie, pois a thread que aguarda o
término de outra (a que fez cjoin) não recupera nenhuma informação de retorno proveniente da thread aguardada. */
//REFAZER
int cjoin(int tid){
	TCB_t *threadEsperada;

	//printf("ThreadAtual->tid: %d, ticket: %d \n", threadAtual->tid, threadAtual->ticket);


	if(threadAtual == NULL || threadAtual->tid == tid){
		return -1;
	}

	/* PEGAMOS A THREAD QUE IRÁ BLOQUEAR A THREAD ATUAL */
	threadEsperada = findThread(tid);
	

	/* SE A THREAD A SER ESPERADA JA ESTA BLOQUEANDO ALGUEM OU É NULA, TERMINA A FUNÇÃO */
	if(threadEsperada == NULL || threadEsperada->state > -1){
		return -1;
	}
	

	/* THREADESPERADA IRÁ BLOQUEAR THREADATUAL */
	threadEsperada->state = threadAtual->tid;
	//printf("ThreadEsperada->tid: %d, ticket: %d State: %d\n", threadEsperada->tid, threadEsperada->ticket, threadEsperada->state);
	
	if(AppendFila2(&filaBloqueado, threadAtual)){ //salva a thread criada na fila de bloqueados
		printf("Append deu merda\n");
		return -1;
	}
	naoDesbloqueia = 1;
	
	swapcontext(&threadAtual->context, contextoEscalonador);

	return 0;
}

/* FUNÇÃO QUE DEVOLVE UMA THREAD DE TID SOLICITADO */
TCB_t* findThread(int tid){
	TCB_t *aux = malloc(sizeof(TCB_t));
	FirstFila2(&filaApto);
	aux = GetAtIteratorFila2(&filaApto);
	while(aux != NULL){
		if(tid == aux->tid){
			return aux;
		}
		NextFila2(&filaApto);
		aux = GetAtIteratorFila2(&filaApto);
	}
	return NULL;
}

/*Inicialização de semáforo: a função csem_init inicializa uma variável do tipo csem_t e consiste em fornecer um valor
inteiro (count), positivo ou negativo, que representa a quantidade existente do recurso controlado pelo semáforo. Para
realizar exclusão mútua, esse valor inicial da variável semáforo deve ser 1. Ainda, cada variável semáforo deve ter
associado uma estrutura que registre as threads que estão bloqueadas, esperando por sua liberação. Na inicialização essa
lista deve estar vazia. */
int csem_init(csem_t *sem, int count){
	initLib();
	sem->fila = NULL;
	sem->count = 1;
	semaforoInit = 1;
	return 0;
}

/*Solicitação (alocação) de recurso: a primitiva cwait será usada para solicitar um recurso. Se o recurso estiver livre, ele é
atribuído a thread, que continuará a sua execução normalmente, caso contrário a thread será bloqueada e posta a espera
desse recurso na fila. Se na chamada da função o valor de count for menor ou igual a zero a thread deverá ser posta no
estado bloqueado e colocada na fila associada a variável semáforo. Para cada chamada a cwait a variável count da
estrutura semáforo é decrementada de uma unidade. */
int cwait(csem_t *sem){
	if(semaforoInit == 0){
		return -1;
	}

	sem->count--;

	TCB_t *threadAnterior = NULL;

	if(sem->fila == NULL){
		sem->fila = malloc(sizeof(FILA2));
		CreateFila2(sem->fila);
	}
	//printf("Count do cwait: %d\n", sem->count);
	if(sem->count < 0){
		threadAnterior = threadAtual;

		if(AppendFila2(sem->fila, threadAnterior)){ //salva a thread criada na fila de bloqueados do semaforo
			printf("Append deu merda\n");
			return -1;
		}
		naoDesbloqueia = 1;
		swapcontext(&threadAnterior->context, contextoEscalonador);
	}

	/* IRÁ ENTRAR PARA A SEÇÃO CRITICA JÁ QUE SAIU DO LOCK */
	//printf("Entrei a primeira vez no cwait \n");
	return 0;
}


/*Liberação de recurso: a chamada csignal serve para indicar que a thread está liberando o recurso. Para cada chamada a
csignal a variável count deverá ser incrementada de uma unidade. Se houver mais de uma thread bloqueada a espera
desse recurso a primeira delas, segundo uma política de FIFO, deverá passar para o estado apto e as demais devem
continuar no estado bloqueado. */
int csignal(csem_t *sem){
	if(semaforoInit == 0){
		return -1;
	}

	sem->count++;
	//printf("Count do csignal: %d \n", sem->count);
	if(sem->count <= 0){
		if(FirstFila2(sem->fila) == 0){
			TCB_t* Thread = (TCB_t*)GetAtIteratorFila2(sem->fila);
			if(AppendFila2(&filaApto, (void *)Thread)){ //salva a thread criada na fila de aptos do semaforo
				printf("Append deu merda\n");
				return -1;
			}
			DeleteAtIteratorFila2(sem->fila);
		}
		if (FirstFila2(sem->fila) != 0){
				free(sem->fila);
				sem->fila = 0;
		}
	}
	return 0;
}

/*Identificação do grupo: Além das funções de manipulação das threads e de sincronização a biblioteca deverá prover a
implementação de uma função que forneça o nome dos alunos integrantes do grupo que desenvolveu a biblioteca
chtread. */
int cidentify (){
    char *name = "\nDevelopers: Cassiano Jaeger Stradolini\n	    & Raphael Piegas Cigana\nVersion: 29/09/2016\n";
    printf("%s\n", name);
    
    return 0;
}







